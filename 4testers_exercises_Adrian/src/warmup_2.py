def square_number(input_number):
    return input_number ** 2


def calculate_celsius_to_fahrenheit(temp_celsius):
    return 32 + temp_celsius * (9 / 5)


def volume_of_cuboid(a, b, h):
    return a * b * h


if __name__ == '__main__':
    # Zadanie 1
    square_1 = square_number(0)
    square_2 = square_number(16)
    square_3 = square_number(2.55)

    print(square_1)
    print(square_2)
    print(square_3)

    # Zadanie 2
    print(calculate_celsius_to_fahrenheit(20))

    # Zadanie 3
    print(volume_of_cuboid(3, 5, 7))
