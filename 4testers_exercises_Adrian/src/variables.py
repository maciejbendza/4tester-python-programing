first_name = "Maciej"
last_name = "Bendza"
age = 24

print(first_name)
print(last_name)
print(age)

# I'm defining a set of variables desribing my dog

name = "Panela"
coat_color = "Black/Brown"
age_in_months = 5
number_of_paws = 4
weight = 5.1
is_male = False
has_chip = True
is_vaccinated = True

print(name, age_in_months, weight, number_of_paws, coat_color, is_male)

friend_name = "Krzysztof"
friend_age = 24
friend_number_of_pets = 1
friend_own_driving_licence = True
friend_years_together = 20.5
print(friend_name, friend_age, friend_number_of_pets, friend_own_driving_licence, friend_years_together,sep=";")



