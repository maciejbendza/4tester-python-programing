import random
import string


def count_average_of_numbers(input_list):
    return sum(input_list) / len(input_list)


def count_average_of_two_numbers(a, b):
    return (a + b) / 2


if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    febuary = [-13, -9, -3, 3]
    january_average = count_average_of_numbers(january)
    febuary_average = count_average_of_numbers(febuary)

    bimonthly_average = count_average_of_two_numbers(january_average, febuary_average)
    print(bimonthly_average)


def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    print("Random string of length", length, "is:", result_str)


#####


def print_player_description():
    player_info = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000,
    }
    nick=player_info["nick"]
    type=player_info["type"]
    exp=player_info["exp_points"]
    print(f"The player {nick} is of type {type} and has {exp} EXP")

print_player_description()

x=input("Enter your nick:")
y=input("Enter your class type:")
z=input("Enter your EXP level:")
print("Hello,"+ x + " the " + y + " with the level of experience " + z)

