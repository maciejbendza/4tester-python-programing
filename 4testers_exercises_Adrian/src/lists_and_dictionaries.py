shopping_list = ['oranges','water','chicken','potatoes','washing liquid']
print(shopping_list[0])
print(shopping_list[1])
print(shopping_list[2])
print(shopping_list[-1])
shopping_list.append('lemons')
print(shopping_list)
print(shopping_list[-1])

number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy)

first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)


animal = {
    "name" : "burek",
    "kind" : "dog",
    "male" : True,
"age": 7
}

dog_age = animal["age"]
print("dog age:", dog_age)