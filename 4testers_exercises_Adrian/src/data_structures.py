movies = ["Dune", "Star Wars", "Blade Runner", "Stalker", "Lost Highway"]
last_movie = movies[-1]
movies.append("LOTR")
movies.append("Titanic")

print(len(movies))

middle_movies = movies[2:5]

print(middle_movies)

movies.insert(0, "Top Gun 2")
print(movies)

###Zadanko
emails = ["a@example.com", "b@example.com"]
if __name__ == '__main__':
    print(len(emails))
    print(emails[0])
    print(emails[-1])
    emails.append("cde@example.com")
    print(emails)

###
friend = {
    "name": "Dawid",
    "age": 24,
    "hobby": ["swimming", "climbing"],
}

friend_hobbies = friend["hobby"]

print("Hobbies of my friend",friend_hobbies)



friend["married"] = True
friend["age"] = 33
print(friend)

