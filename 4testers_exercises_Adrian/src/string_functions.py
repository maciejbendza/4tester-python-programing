def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widzieć w naszym mieście: {city}!")
def email_generator(imie, nazwisko):
    print(f"{imie.lower()}.{nazwisko.lower()}@4testers.pl")

if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Kasia", "Szczecin")

    email_generator("Janusz", "Nowak")
