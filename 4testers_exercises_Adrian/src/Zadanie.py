import random
from dateutil.utils import today

list_of_women_names = ["Roman", "Dariusz", "Maciej", "Kamil", "Robert"]
list_of_men_names = ["Joanna", "Renata", "Gabriela", "Agata", "Ludwika"]
list_of_last_names = ["Nowak", "Dorsz", "Wrzos", "Krupy", "Walka", "Rzepa", "Romaniuk", "Werda"]
list_of_countries = ["Poland", "Germany", "France", "Czech Republic", "Slovakia", "Sweden"]



def generate_random_first_name(list_of_names):
    random_name = random.choice(list_of_names)
    return random_name


def generate_random_last_name():
    random_last_name = random.choice(list_of_last_names)
    return random_last_name


def generate_random_country():
    random_country = random.choice(list_of_countries)
    return random_country


def generate_random_email():
    return f'{generate_random_first_name()}.{generate_random_last_name()}@example.com'


def print_personal_data(personal_information_dicitionary):
    print(
        f"Hi! I'm {personal_information_dicitionary['firstname']} {personal_information_dicitionary['lastname']}. I come from {personal_information_dicitionary['country']} and"
        f" I was born in {personal_information_dicitionary['year_of_birth']}.")


def print_list_of_dictionaries(ammount_of_dictionaries):
    print(list_of_dictionaries[ammount_of_dictionaries])


list_of_dictionaries = []
def generate_dicitonary_with_personal_data():
    for i in range(0, x):
        first_name = generate_random_first_name(list_of_women_names)
        last_name = generate_random_last_name()
        country = generate_random_country()
        email = f'{first_name}.{last_name}@example.com'
        lower_case_email = email.lower()
        age = random.randint(5, 45)
        is_adult = age >= 18
        year_of_birth = today().year - age
        personal_information_dicitionary = {
            "firstname": first_name,
            "lastname": last_name,
            "country": country,
            "email": lower_case_email,
            "age": age,
            "adult": is_adult,
            "year_of_birth": year_of_birth
        }
        list_of_dictionaries.append(personal_information_dicitionary)
        first_name = generate_random_first_name(list_of_men_names)
        last_name = generate_random_last_name()
        country = generate_random_country()
        email = f'{first_name}.{last_name}@example.com'
        lower_case_email = email.lower()
        age = random.randint(5, 45)
        is_adult = age >= 18
        year_of_birth = today().year - age
        personal_information_dicitionary = {
            "firstname": first_name,
            "lastname": last_name,
            "country": country,
            "email": lower_case_email,
            "age": age,
            "adult": is_adult,
            "year_of_birth": year_of_birth
        }
        list_of_dictionaries.append(personal_information_dicitionary)



if __name__ == '__main__':
    print("Type ammount of results you would like to reach:")
    x = int(input())
    generate_dicitonary_with_personal_data()
    for i in range(0, x):
        print_personal_data(list_of_dictionaries[i])
        print_list_of_dictionaries(i)
