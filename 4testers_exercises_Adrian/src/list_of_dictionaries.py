animal = {
    "kind": "dog",
    "age": 2,
    "male": True
}
animal2 = {
    "kind": "cat",
    "age": 5,
    "male": False
}

animal_kinds = ["dog", "cat", "fish"]

animals = [
{
    "kind": "cat",
    "age": 5,
    "male": False
},
{
    "kind": "dog",
    "age": 2,
    "male": True
},
{
    "kind": "fish",
    "age": 1,
    "male": True
}
]

last_animal = animals[-1]
last_animal_age = last_animal['age']
print(last_animal_age)