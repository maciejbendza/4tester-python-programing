
first_name = "Maciej"
last_name = "Bendza"
email = "maciej.bendza@gmail.com"

print("Mam na imię ", first_name,".", " Moje nazwisko to ",last_name,".", sep="")

my_bio = "Mam na imię " + first_name +"." + " Moje nazwisko to " + last_name + "." + " Mój mail to " + email
print(my_bio)
#F- string

my_bio_using_f_string = f"Mam na imie {first_name}. Moje nazwisko to {last_name}. Mój email to {email}."
print(my_bio_using_f_string)

print(f"wynik mnożenia 4 przez 5 to {4*5}")

### Algebra

area_of_a_circle_with_radius_5 = 3.1415 * 5 **2
print(f"Area of circle with radius 5 :{area_of_a_circle_with_radius_5}")
