from src.word_analytics import filter_words_containing_letter_a

def test_filtering_words_for_empty_list():
    filtered_list = filter_words_containing_letter_a([])
    assert filtered_list == []


