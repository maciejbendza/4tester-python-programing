from src.emloyee_management import get_dictionary_with_random_personal_data


def test_single_Employee_generation_seniority_years_have_correct_value():
    test_employee = get_dictionary_with_random_personal_data()
    employee_seniority_years = test_employee["seniority_years"]
    assert employee_seniority_years >= 0
    assert employee_seniority_years <= 40
    assert isinstance(employee_seniority_years, int)
def test_single_employee_generation_has_proper_keys():
    test_employee = get_dictionary_with_random_personal_data()
    assert "email" in test_employee.keys()
    assert "seniority_years" in test_employee.keys()
    assert "female" in test_employee.keys()